Artis
=====

|pypi version| |pipeline status| |coverage report| |code style| |imports| |GPLv3 license| |readthedocs|

Seamless Integration of numerical computing frameworks.

Description
-----------

**Artis** is a framework enabling effortless code integration with popular numerical computing frameworks such as *NumPy, Cupy, PyTorch, TensorFlow, JAX,* and more. **Artis** maintains *NumPy*'s familiar API for ndarray operations while leveraging functionalities of many frameworks and allowing cross-device compatibility for CPU, GPU, or TPU.

Source code
-----------

**Artis** is open source and is available at GitLab_

Installation
------------

It is available at PyPi_ and can be installed with pip as:

.. code-block:: bash

    $ pip install artis

Documentation
-------------

For documentation, ReadTheDocs_


License
-------

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.


.. _GitLab: https://gitlab.com/scimet/artis
.. _PyPi: https://pypi.org/project/artis
.. _ReadTheDocs: https://artis.readthedocs.io
.. |pypi version| image:: https://img.shields.io/pypi/v/artis
   :target: https://pypi.org/project/artis
   :alt: PyPI
.. |pipeline status| image:: https://gitlab.com/scimet/artis/badges/main/pipeline.svg
   :target: https://gitlab.com/scimet/artis/-/commits/main
   :alt: Pipeline status
.. |coverage report| image:: https://gitlab.com/scimet/artis/badges/main/coverage.svg
   :target: https://gitlab.com/scimet/artis/-/commits/main
   :alt: Coverage
.. |code style| image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/psf/black
   :alt: Black
.. |imports| image:: https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336
   :target: https://timothycrosley.github.io/isort
   :alt: Isort
.. |GPLv3 license| image:: https://img.shields.io/badge/License-GPLv3-blue.svg
   :target: https://www.gnu.org/licenses/gpl-3.0
   :alt: GPLv3 license
.. |readthedocs| image:: https://readthedocs.org/projects/artis/badge/?version=stable
   :target: https://artis.readthedocs.io/en/stable
   :alt: Documentation Status
