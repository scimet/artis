2023-08-04  Gabriel Jover-Manas  <gjover@cells.es>

	Add loading modules and euler module example

	Fix error passing list as array

	Fix find_frame returning device and framework instead of device and backend

	Refactoring backend class names

2023-08-03  Gabriel Jover-Manas  <gjover@cells.es>

	test: Add frame initialization test

	Fix array.to_device using the right backend

	Fix torch array type

	test: Add backend fixture

	test: Replace framework_device_array2 by framework_device_array_dest

2023-08-02  Gabriel Jover-Manas  <gjover@cells.es>

	Refactoring framework to backend

	Merge branch 'doc' into devel

	doc: Use numpydoc instead of autodoc

2023-08-01  Gabriel Jover-Manas  <gjover@cells.es>

	Update README.rst

2023-07-31  Gabriel Jover-Manas  <gjover@cells.es>

	Add documentation CI

2023-06-19  Joaquin Oton  <joaquin.oton@gmail.com>

	Merge branch 'devel' of git.cells.es:scimet/artis into devel

	Fixed bug when no device/framework are passed in Array constructor

	Fixed setting temporary device and framework of input array

	Fixed redirection of attributes setting

	Detach torch tensors before converting tu numpy, if required

2023-06-19  Joaquín Otón Pérez  <joton@cells.es>

	Update README.rst

	Update README.rst

	Update README.rst

	Update README.rst

	Update README.rst

	Update README.rst

	Update README.rst

	Update README.rst

	Update README.rst

2023-06-16  Joaquín Otón Pérez  <joton@cells.es>

	Merge branch 'devel' into 'main'
	Torch plugin added #6

	See merge request scimet/artis!3

2023-06-16  Joaquin Oton  <joaquin.oton@gmail.com>

	Torch plugin added #6

2023-06-16  Joaquín Otón Pérez  <joton@cells.es>

	Merge branch 'devel' into 'main'
	Working version prior to Torch plugin

	See merge request scimet/artis!2

2023-06-16  Joaquin Oton  <joaquin.oton@gmail.com>

	Fixed baseframework test for new method from_array

	Added from_array to baseframework class and overloaded in frameworks as normal method

	Added __iter__ method to ArtisArray

	Removed debugging message

	Removed debugging message

	Call to convert among arrays unified in from_array method

	For Array/Tensor objects without __ufuncs__ interface, we implement this interface in the framework

	Use hasattr instead of find in dir

	get_item also returns an Artis Array

	Overloading constructor to separate input frame case from standard device/framework to ease the latter call.

2023-06-15  Joaquín Otón Pérez  <joton@cells.es>

	Merge branch 'devel' into 'main'
	1st working version of both Frame and Array.

	Closes #3

	See merge request scimet/artis!1

2023-06-15  Joaquin Oton  <joaquin.oton@gmail.com>

	To_device tests crossing all combinations of device/framework

	To_device test fixed. There is not function return.

	Added second framewor_device_array2 fixture to calculate crossed tests.

	ArtisArray constructor shouldn't call internally another ArtisArray object to be constructed itself.
	If input is an external array or another ArtisArray it will be converted to device/frame if passed, otherwise keep their origin.

	Arrays should be converterd to self.frame but not within another ArtisArray. Fixed

	Arrays were not actually extracted from ArtisArray. Fixed

	to_device changes internal array. Nothing to return

	Added equal comparison method

	Default framework is selected by find_frame regarding device

2023-06-14  Joaquin Oton  <joaquin.oton@gmail.com>

	Fixed issue #3

	Fixed flake8 comment complain

	Extract and remove item from dict in one single step

	Merge branch 'devel' of git.cells.es:scimet/artis into devel

2023-06-10  Gabriel Jover-Manas  <gjover@cells.es>

	Merge branch 'devel'

2023-06-09  Gabriel Jover-Manas  <gjover@cells.es>

	Fix array to device error when framework is not defined
	When changing to a new device, now, the default framework
	is used if the new framework is not specified.

	Fixes #2 Default framework name not used when selecting device

2023-06-09  Joaquin Oton  <joaquin.oton@gmail.com>

	Merge branch 'devel' of git.cells.es:scimet/artis into devel

2023-06-09  Gabriel Jover-Manas  <gjover@cells.es>

	Merge branch 'devel'

	ci:Add coverage

	Fix entrypoint loading error on missing package

2023-06-08  Gabriel Jover-Manas  <gjover@cells.es>

	Add setup requirements

	Add specific gitlab cicd

	Merge branch 'devel'

	Sort methods

	Add call to base framework and _get_result to the implementations

	Fix artis frame and array reperesntations

	Add returning artis array

2023-06-08  Joaquin Oton  <joaquin.oton@gmail.com>

	Merge branch 'devel' of git.cells.es:scimet/artis into devel

2023-06-08  Gabriel Jover-Manas  <gjover@cells.es>

	Add find_frame
	find frame find right device - framework pair

2023-06-07  Gabriel Jover-Manas  <gjover@cells.es>

	Move to_device method from frame to array

	Add Artis Array

	test: Add parametrized fixures

	Simplify import from cupy to numpy

2023-06-06  Gabriel Jover-Manas  <gjover@cells.es>

	Add default framework for device

	Fix to_device forcing numpy

	Fix getattr returning a frame with different framework

	Fix frame dir missing self methods

	test: Rename base framework test

2023-06-06  Joaquin Oton  <joaquin.oton@gmail.com>

	Merge branch 'devel' of git.cells.es:scimet/artis into devel

2023-06-02  Gabriel Jover-Manas  <gjover@cells.es>

	pylint: Fix builtins configuration

	test: Add testing for framework from_ attribute

	Fix frame to_device method

	Add cupy framework

	test: Add list of tuples (framework device array) for testing

	test: Add test parameters

	Remove attribute list clearing

	Add base framework test

	Move base framework

	Move frame tests

	Move framework catalog to main artis

2023-06-01  Gabriel Jover-Manas  <gjover@cells.es>

	Move framework discovery to artis main module

2023-06-01  Joaquin Oton  <joaquin.oton@gmail.com>

	Merge branch 'devel' of git.cells.es:scimet/artis into devel

2023-06-01  Gabriel Jover-Manas  <gjover@cells.es>

	Remove frame default dict

2023-05-31  Gabriel Jover-Manas  <gjover@cells.es>

	Fix __getattr__ returning a new Frame

	test: Add frame getattr test

	Fix __getattr__ returning a new Frame

	Add list of attributes to frame print

	Add creating frame from frame

	env: Add git

	Remove artis.io

	test: Add dir and str frame asserts

	Add artis as frame

	Fix call to _recursive_getattr in __dir__

2023-05-30  Joaquin Oton  <joaquin.oton@gmail.com>

	Updated to new Framework style

	Fixed typo

	quick fix

2023-05-30  Gabriel Jover-Manas  <gjover@cells.es>

	Add Frame and Frameworks

2023-05-29  Gabriel Jover-Manas  <gjover@cells.es>

	Add Frame tests

	pre-commit:Fix flake8 url

2022-10-11  Gabriel Jover-Manas  <gjover@cells.es>

	Add tests

	Fix style

	Set artis as package instead of namespace

2022-09-06  Gabriel Jover-Manas  <gjover@cells.es>

	Add io

2022-09-06  Joaquin Oton  <joaquin.oton@gmail.com>

	License updated

2022-09-06  Gabriel Jover-Manas  <gjover@cells.es>

	Fix style

2022-09-06  Joaquin Oton  <joaquin.oton@gmail.com>

	First commit

	First coockiecutter hook
