# Copyright (C) 2017 Joaquín Otón
#
# Authors: Joaquín Otón, Gabriel Jover Mañas
#
# This file is part of artis
#
# artis is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# artis is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with artis.  If not, see <http://www.gnu.org/licenses/>.
"""
Artis, seamless Integration of numerical computing frameworks.

**Artis** is a framework enabling effortless code integration with popular
numerical computing frameworks such as *NumPy, Cupy, PyTorch, TensorFlow,
JAX,* and more. **Artis** maintains *NumPy*'s familiar API for ndarray
operations while leveraging functionalities of many frameworks and allowing
cross-device compatibility for CPU, GPU, or TPU.

Attributes
----------
    frame: Frame
        Artis main frame
    framework_catalog: dic
        Dictionary of registered framework backends
    device_frameworks: dict
        Dictionary mapping available frameworks for each device
    array_backend: dict
        Dictionary mapping available backends for each array class
"""
import sys
from typing import Dict, List

from artis import _version
from artis.framework import Array, BaseBackend, Frame

if sys.version_info < (3, 10):
    from importlib_metadata import entry_points
else:
    from importlib.metadata import entry_points

__all__ = ["Frame", "Array"]

__version__ = _version.get_versions()["version"]


# Frameworks catalogs
framework_catalog: Dict[str, BaseBackend] = {}
framework_modules: Dict[str, Dict[str, BaseBackend]] = {}
device_frameworks: Dict[str, List[str]] = {}
array_backend: Dict[type, BaseBackend] = {}
default_framework_for_device: Dict[str, str] = {}


def add_framework(backend):
    """
    Register a framework backend artis catalogs.

    Artis has four catalogs mapping the relations between framework names,
    backends, devices and arrays.
    framework_catalog maps the framework name with the backend.
    device_frameworks maps the device with the name of available frameworks.
    array_backend maps the array types with the corresponding framework backend.
    default_framework_for_device maps the device with it's default framework
    name.

    Parameters
    ----------
    backend : BaseBackend
        Framework backend to be registered.
    """
    framework_catalog[backend.name] = backend
    array_backend[backend.array_class] = backend

    for device in backend.devices:
        if device not in device_frameworks:
            device_frameworks[device] = []
        device_frameworks[device].append(backend.name)
        if device not in default_framework_for_device:
            default_framework_for_device[device] = backend.name


def load_entry_points():
    """Load entry points."""
    for framework_ep in entry_points(group="artis_frameworks"):  # type: ignore
        try:
            name = framework_ep.name
            module, framework = name.split(".")
            backend = framework_ep.load()  # type: ignore
            if module == "default":
                add_framework(backend())
            else:
                if framework not in framework_modules:
                    framework_modules[framework] = {}
                framework_modules[framework][module] = backend()

        except (ImportError, ModuleNotFoundError):
            pass


load_entry_points()


# Artis main frame
# Direct calls to artis will be handled with the main frame
frame = Frame()  # Artis main frame


def __getattr__(attr):  # numpydoc ignore=PR01,RT01
    """Redirect attributes access to the main frame."""
    if attr in dir(frame):
        return getattr(frame, attr)

    raise AttributeError(
        f"module {frame.backend.module} has no attribute {attr}"
    )
