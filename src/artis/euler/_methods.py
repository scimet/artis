# Copyright (C) 2017 Joaquín Otón
#
# Authors: Joaquín Otón, Gabriel Jover Mañas
#
# This file is part of artis
#
# artis is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# artis is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with artis.  If not, see <http://www.gnu.org/licenses/>.
"""Euler angle conversion methods."""


def rot_x(alpha, frame=None):
    """
    Euler rotation matrix for X axis.

    Parameters
    ----------
    alpha : number
        X axis rotation angle.
    frame : frame
        Artis frame for the calculations.

    Returns
    -------
    array
        Euler X axis rotaion matrix (3x3).
    """
    sina = frame.sin(alpha)
    cosa = frame.cos(alpha)
    out = frame.array([[1, 0, 0], [0, cosa, -sina], [0, sina, cosa]])
    return out
