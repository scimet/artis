# Copyright (C) 2017 Joaquín Otón
#
# Authors: Joaquín Otón, Gabriel Jover Mañas
#
# This file is part of artis
#
# artis is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# artis is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with artis.  If not, see <http://www.gnu.org/licenses/>.
"""Cupy backend interface for Euler angle conversions."""

from artis.euler import _methods
from artis.framework import CupyBackend, Frame


class EulerCupyBackend(CupyBackend):
    """Cupy backend for euler calculations."""

    def _get_result(
        self, attrib_list, device, *args, **kwargs
    ):  # numpydoc ignore=PR01,RT01
        """Get operation result from the framwework."""
        kwargs["frame"] = Frame(framework=self.name, device=device)
        return self._recursive_getattr(_methods, attrib_list)(*args, **kwargs)
