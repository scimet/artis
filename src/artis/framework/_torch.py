# Copyright (C) 2017 Joaquín Otón
#
# Authors: Joaquín Otón, Gabriel Jover Mañas
#
# This file is part of artis
#
# artis is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# artis is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with artis.  If not, see <http://www.gnu.org/licenses/>.
"""Torch backend interface."""

from contextlib import contextmanager

import torch as _tr  # pylint: disable=import-error

import artis
from artis.framework import BaseBackend

# Check if context manager issue in pytoch has finally been included :
# https://github.com/pytorch/pytorch/issues/82296
# pylint: disable=maybe-no-member
_DEVICE_CONSTRUCTOR = {
    # standard ones
    _tr.empty,
    _tr.empty_strided,
    _tr.empty_quantized,
    _tr.ones,
    _tr.arange,
    _tr.bartlett_window,
    _tr.blackman_window,
    _tr.eye,
    _tr.fft.fftfreq,
    _tr.fft.rfftfreq,
    _tr.full,
    _tr.fill,
    _tr.hamming_window,
    _tr.hann_window,
    _tr.kaiser_window,
    _tr.linspace,
    _tr.logspace,
    # _tr.nested_tensor,
    # _tr.normal,
    _tr.ones,
    _tr.rand,
    _tr.randn,
    _tr.randint,
    _tr.randperm,
    _tr.range,
    _tr.sparse_coo_tensor,
    _tr.sparse_compressed_tensor,
    _tr.sparse_csr_tensor,
    _tr.sparse_csc_tensor,
    _tr.sparse_bsr_tensor,
    _tr.sparse_bsc_tensor,
    _tr.tril_indices,
    _tr.triu_indices,
    _tr.vander,
    _tr.zeros,
    _tr.asarray,
    # weird ones
    _tr.tensor,
    _tr.as_tensor,
}

_NUMPY_API_TENSOR_WRAPPER = {
    "array": _tr.asarray,  # Called from backend
    "iscomplex": _tr.is_complex,  # Called from backend
    "power": _tr.pow,
    "random.rand": _tr.rand,
    "equal": _tr.Tensor.__eq__,  # Called from Tensor class
}


class _DeviceMode(_tr.overrides.TorchFunctionMode):
    """
    Object to build a context manager calling push method.

    Parameters
    ----------
    device : Torch device
        Torch device.
    """

    def __init__(self, device):
        """
        Initialize the torch device.

        Parameters
        ----------
        device : str
            Device name.
        """
        self.device = _tr.device(device)

    # pylint: disable=unused-argument
    def __torch_function__(
        self, func, types, *args, **kwargs
    ):  # numpydoc ignore=PR01,RT01

        """Execute function with the mode device."""

        if func in _DEVICE_CONSTRUCTOR and kwargs.get("device") is None:
            kwargs["device"] = self.device

        return func(*args, **kwargs)


class TorchBackend(BaseBackend):
    """
    Torch backend.
    """

    _name = "torch"
    _module = _tr
    _array_class = _tr.Tensor
    # pylint: disable=c-extension-no-member
    _devices = [f"cuda:{n}" for n in range(_tr.cuda.device_count())]
    _devices += ["cpu"]

    def get_device(self, array: _tr.Tensor):
        """
        Get device name from array.

        Parameters
        ----------
        array : ndarray
            Torch ndarray.

        Returns
        -------
        str
            Device name.
        """
        return array.device.type

    def from_array(self, array, backend=None, device=None, dtype=None):
        """
        Convert any backend array to Torch tensor.

        Parameters
        ----------
        array : ndarray
            Input array related to any backend.
        backend : BaseBackend
            Framework backend of the input array. To be considered for
            conversion.
        device : str
            Target device for this backend.
        dtype : TYPE, optional
            Dtype for output array. The default is None.

        Returns
        -------
        ndarray
            Copy or view of input array, depending on the backend.
        """
        if device not in self._devices:
            raise ValueError(
                f"Device {device} not available in Torch. "
                f"Expected one of {self._devices}"
            )
        return _tr.asarray(array, device=device, dtype=dtype)

    def _get_result(self, attrib_list, device, *args, **kwargs):
        """
        Get operation result from the framwework.

        Method nested name is given by attrib_list. It uses the selected device
        with passed arguments.

        Parameters
        ----------
        attrib_list : list
            List of nested attributes.
        device : str
            Device name.
        *args : list
            Function arguments.
        **kwargs : dic
            Function keyword arguments.

        Returns
        -------
        any
            Operation's result.
        """

        func_module_str = ".".join(attrib_list)
        func = _NUMPY_API_TENSOR_WRAPPER.get(func_module_str)

        if func is None:
            func = self._recursive_getattr(_tr, attrib_list)

        if func in _DEVICE_CONSTRUCTOR:
            if kwargs.get("device") is None:
                kwargs["device"] = device

        # convert list to arrays
        for arg in args:
            new_args = ()
            if isinstance(arg, list):
                arg = self.list_to_tensor(arg)
            new_args += (arg,)
        args = new_args

        return func(*args, **kwargs)

    def __array_ufunc__(
        self, array, ufunc, _method, *inputs, **kwargs
    ):  # numpydoc ignore=PR01,RT01

        """Array universal function."""

        func_name = ufunc.__name__
        if hasattr(ufunc, "__module__"):
            module = ufunc.__module__.split(".")[1:]
        else:
            module = []

        func_module = module + [func_name]
        func_module_str = ".".join(func_module)

        if "out" in kwargs:
            kwargs["out"] = kwargs["out"][0]

        func = _NUMPY_API_TENSOR_WRAPPER.get(func_module_str)
        if func is None:
            func = self._recursive_getattr(_tr, func_module)

        newinputs = ()
        for obj in inputs:
            # convert to own device (to assert comparison to non torch arrays)
            if (
                not isinstance(obj, _tr.Tensor)
                and type(obj) in artis.array_backend
            ):
                obj = _tr.asarray(obj, device=array.device.type)

            newinputs += (obj,)

        return func(*newinputs, **kwargs)

    def __array_function__(
        self, array, func, _types, *args, **kwargs
    ):  # numpydoc ignore=PR01,RT01

        """Array function dunder."""
        args = args[0]
        return self.__array_ufunc__(array, func, None, *args, **kwargs)

    @contextmanager
    @classmethod
    def device_mode(cls, device):
        """
        Set a context manager for a specific device.

        Select default device to work in a block. Torch does not include it.

        Parameters
        ----------
        device : str
            Device name.

        Notes
        -----
        Not all functions are acepted. Check
        torch.overrides.get_ignored_functions() to list ignored functions.
        """
        if device in cls._devices:
            with _DeviceMode.push(_tr.device(device)):
                yield

    def list_to_tensor(self, list_):
        """
        Convert nested lists to tensor.

        Parameters
        ----------
        list_ : list
            List to be converted to tensor.

        Returns
        -------
        tensor
            Tensor made by stacking list elements.
        """
        stack = []
        for value in list_:
            print(value)
            if isinstance(value, list):
                value = self.list_to_tensor(value)
            elif not isinstance(value, _tr.Tensor):
                value = _tr.tenso(value)
            stack.append(value)
        if len(stack) > 0:
            tensor = _tr.stack(stack)
        else:
            tensor = _tr.asarray([])
        return tensor
