# Copyright (C) 2017 Joaquín Otón
#
# Authors: Joaquín Otón, Gabriel Jover Mañas
#
# This file is part of artis
#
# artis is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# artis is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with artis.  If not, see <http://www.gnu.org/licenses/>.
"""Numpy backend interface."""

import numpy as _np

from artis.framework import BaseBackend


class NumpyBackend(BaseBackend):
    """
    Numpy backend.
    """

    _name = "numpy"
    _module = _np
    _array_class = _np.ndarray
    _devices = ["cpu"]

    # pylint: disable=unused-argument
    def get_device(self, array: _np.ndarray):
        """
        Get device name from array.

        Parameters
        ----------
        array : ndarray
            Numpy ndarray.

        Returns
        -------
        str
            Device name.
        """
        return self._devices[0]

    def from_array(self, array, backend=None, device=None, dtype=None):
        """
        Convert any backend array to Numpy array.

        Parameters
        ----------
        array : ndarray
            Input array related to any backend.
        backend : BaseBackend
            Framework backend of the input array. To be considered for
            conversion.
        device : str
            Target device for this backend. In this case, Numpy, only accepts
            'cpu' devices.
        dtype : TYPE, optional
            Dtype for output array. The default is None.

        Returns
        -------
        ndarray
            Copy or view of input array, depending on the backend.
        """
        if backend.name == "cupy":
            # backend = artis.framework_catalog["cupy"]
            output = backend.module.asnumpy(array)
            if dtype is not None:
                output.astype(dtype)
            return output
        elif backend.name == "torch":
            if array.requires_grad:
                return _np.asarray(array.detach().cpu(), dtype)
            else:
                return _np.asarray(array.cpu(), dtype)
        elif device not in [None, "cpu"]:
            raise NotImplementedError(
                f"Numpy does not support {device} device."
            )
        else:
            return _np.asarray(array, dtype)

    def _get_result(self, attrib_list, device, *args, **kwargs):
        """
        Get operation result from the framwework.

        Method nested name is given by attrib_list. It uses the selected device
        with passed arguments.

        Parameters
        ----------
        attrib_list : list
            List of nested attributes.
        device : str
            Device name.
        *args : list
            Function arguments.
        **kwargs : dic
            Function keyword arguments.

        Returns
        -------
        any
            Operation's result.
        """
        return self._recursive_getattr(_np, attrib_list)(*args, **kwargs)
