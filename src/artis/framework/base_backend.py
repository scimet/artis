# Copyright (C) 2017 Joaquín Otón
#
# Authors: Joaquín Otón, Gabriel Jover Mañas
#
# This file is part of artis
#
# artis is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# artis is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with artis.  If not, see <http://www.gnu.org/licenses/>.
"""
Backend baseclass for mathematical frameworks.
"""

import abc
import functools

import artis


class BaseBackend(abc.ABC):
    """
    Abstract famework backend.

    Derivated backends have to define the attributes:
    _name
    _module
    _array_class
    _devices
    from_
    """

    # Properties
    @property
    def name(self):  # numpydoc ignore=RT01
        """Framework name."""
        return self._name

    @property
    def module(self):  # numpydoc ignore=RT01
        """Backend module."""
        return self._module

    @property
    def array_class(self):  # numpydoc ignore=RT01,SS05
        """Class of the ndarray for this backend."""
        return self._array_class

    @property
    def devices(self):  # numpydoc ignore=RT01
        """List of supported devices."""
        return self._devices

    # class methods
    def __init__(self):
        """Initialize BaseBackend."""
        if not hasattr(self, "_module"):
            raise NotImplementedError(f"{self} should define _module.")
        if not hasattr(self, "_name"):
            raise NotImplementedError(f"{self} should define _name.")
        if not hasattr(self, "_array_class"):
            raise NotImplementedError(f"{self} should define _array_class.")
        if not hasattr(self, "_devices"):
            raise NotImplementedError(f"{self} should define _devices.")
        if not hasattr(self, "from_array"):
            raise NotImplementedError(f"{self} should define from_.")

    def call(self, attrib_list, device, *args, **kwargs):
        """
        Call to backend method.

        This method interfaces the call to any backend method.

        Parameters
        ----------
        attrib_list : list
            List of nested method names to be called.
        device : str
            Device name.
        *args : list
            Function arguments.
        **kwargs : dic
            Function keyword arguments.

        Returns
        -------
        any
            Operation's result.
        """
        args, kwargs = self._extract_arrays(args, kwargs)

        result = self._get_result(attrib_list, device, *args, **kwargs)

        if isinstance(result, self._array_class):
            result = artis.Array(result, device=device, framework=self.name)
        return result

    # abstract methods
    @abc.abstractmethod
    def get_device(self, array):
        """
        Get device name from array.

        Parameters
        ----------
        array : ndarray
            Ndarray.

        Returns
        -------
        str
            Device name.
        """
        raise NotImplementedError(f"{self} does not implement this method.")

    @abc.abstractmethod
    def from_array(self, array, backend=None, device=None, dtype=None):
        """
        Convert any backend array to self array.

        Parameters
        ----------
        array : ndarray
            Input array related to any backend.
        backend : BaseBackend
            Framework backend of the input array. To be considered for
            conversion.
        device : str
            Target device for this backend.
        dtype : TYPE, optional
            Dtype for output array. The default is None.

        Returns
        -------
        ndarray
            Copy or view of input array, depending on the framework backend.
        """
        raise NotImplementedError(f"{self} does not implement this method.")

    @abc.abstractmethod
    def _get_result(self, attrib_list, device, *args, **kwargs):
        """
        Get operation result from the framwework.

        Method nested name is given by attrib_list. It uses the selected device
        with passed arguments.

        Parameters
        ----------
        attrib_list : list
            List of nested attributes.
        device : str
            Device name.
        *args : list
            Function arguments.
        **kwargs : dic
            Function keyword arguments.

        Returns
        -------
        any
            Operation's result.
        """
        raise NotImplementedError(f"{self} does not implement this method.")

    # class customization
    def __getattr__(self, name):  # numpydoc ignore=PR01,RT01
        """Redirect attributes to backend library."""
        try:
            # pylint: disable=no-member
            attr = object.__getattr__(self._module, name)
        except AttributeError as error:
            msg = f"Attribute {name} not found in {self}"
            raise AttributeError(msg) from error

        return attr

    def __dir__(self):  # numpydoc ignore=PR01,RT01
        """Return attributes of backend library."""
        return dir(self.module)

    # helpers
    def _recursive_getattr(self, obj, attr, *args):  # numpydoc ignore=PR01,RT01
        """Recursive call to getattr."""

        def _getattr(obj, attr):  # numpydoc ignore=GL08
            return getattr(obj, attr, *args)

        return functools.reduce(_getattr, [obj] + attr)

    def _extract_arrays(self, args, kwargs):  # numpydoc ignore=PR01
        """
        Extract backend array from arguments and keyword arguments.

        Artis arrays are replaced by the undelying backend array.

        Returns
        -------
        list
            Arguments after converting artis arrays to backend arrays.
        dic
            Keyword arguments after converting artis arrays to backend arrays.
        """
        newargs = ()
        for arg in args:
            if isinstance(arg, artis.Array):
                newargs = newargs + (arg.array,)
            else:
                newargs = newargs + (arg,)
        for key, obj in kwargs.items():
            if isinstance(obj, artis.Array):
                kwargs[key] = obj.array

        return newargs, kwargs
