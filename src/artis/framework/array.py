# Copyright (C) 2017 Joaquín Otón
#
# Authors: Joaquín Otón, Gabriel Jover Mañas
#
# This file is part of artis
#
# artis is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# artis is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with artis.  If not, see <http://www.gnu.org/licenses/>.
"""
Array class to work on any backend.
"""

from numpy.lib import mixins

import artis


# pylint: disable=fixme
# TODO: Add Array.copy
class Array(mixins.NDArrayOperatorsMixin):
    """
    Array class to work on any backend.

    Parameters
    ----------
    array : optional
        Data array used to build the artis array.
    device : optional
        Active device to set the frame.
    framework : optional
        Framework used to set the frame.

    Attributes
    ----------
    array : ndarray
        Backend array used in the frame.
    frame : Frame
        Frame where array operations take place.
    """

    # class methods
    def __init__(self, array=None, device=None, framework=None):
        """
        Initialize the artis array.

        Parameters
        ----------
        array : optional
            Data array used to build the artis array.
        device : optional
            Active device to set the frame.
        framework : optional
            Framework used to set the frame.
        """
        # destination frame
        device_dest, framework_dest = artis.find_frame(device, framework)
        self.frame = artis.Frame(device=device_dest, framework=framework_dest)
        self.array = None

        if array is not None:
            # If the array is a list o tuple, create an array from it
            if type(array) in [list, tuple]:
                self.array = self.frame.backend.module.array(array)
                return
            # If it is an artis array, just copy it
            elif isinstance(array, artis.Array):
                frame = array.frame
                array = array.array
            # Otherwise findout which backend and device to use
            else:
                array_type = type(array)

                if array_type in artis.array_backend:
                    array_backend = artis.array_backend[array_type]
                    array_device = array_backend.get_device(array)
                    frame = artis.Frame(
                        device=array_device, framework=array_backend.name
                    )
                else:
                    raise ValueError(
                        f"No framewok available for {array_type}.\n"
                        f"Please use one of {artis.array_backend.keys()}"
                    )

            if device is None and framework is None:
                self.frame = frame
            elif self.frame != frame:
                array = self.frame.backend.from_array(
                    array, frame.backend, device_dest
                )

            self.array = array

        else:
            # self.frame = frame_dest
            self.array = self.frame.array([])

    def to_device(self, device: str, framework: str = None):
        """
        Send array to a new device.

        Parameters
        ----------
        device : str
            Device name.
        framework : str, optional
            Framework for the new array frame.
        """

        backend = self.frame.backend

        # Change frame to destination
        self.frame.set_device(device, framework)
        self.array = self.frame.backend.from_array(
            self.array, backend, self.frame.device
        )

    def __iter__(self):  # numpydoc ignore=PR01,RT01
        """Iterator dunder."""
        for obj in self.array.__iter__():
            if isinstance(obj, self.array.__class__):
                yield artis.Array(obj)
            else:
                yield obj

    # class customization
    def __getitem__(self, arg):  # numpydoc ignore=PR01,RT01
        """List getter dunder."""
        return artis.Array(self.array[arg])

    def __setitem__(self, arg, value):  # numpydoc ignore=PR01,RT01
        """List setter dunder."""
        self.array[arg] = value

    def __getattr__(self, name):  # numpydoc ignore=PR01,RT01
        """
        Attribute getter.

        Catch the requested attribute. If the backend array has the attribute,
        the returning result is converted to an artis array.
        """
        attrib = getattr(self.array, name)

        if callable(attrib):

            def func(*args, **kwargs):
                args_frame = self.frame.extract_frame(args, kwargs)
                args, kwargs, device, framework = args_frame

                result = attrib(*args, **kwargs)
                return artis.Array(result, device=device, framework=framework)

            return func
        else:
            return attrib

    def __setattr__(self, name, value):  # numpydoc ignore=PR01,RT01
        """
        Attribute setter.

        If the attribut is an artis array attribute it is setted.
        Otherwise the backend array attribute is set.
        """
        if name in ["frame", "array"]:
            self.__dict__[name] = value
        else:
            setattr(self.__dict__["array"], name, value)

    def __dir__(self):  # numpydoc ignore=PR01,RT01
        """
        Return list of attribute names of active framework.

        For Ipython console purposes.
        """
        dir_ = self.array.__dir__()
        dir_.append("to_device")
        return dir_

    def __str__(self):  # numpydoc ignore=RT01
        """Array string formating."""
        str_ = f"Artis Array({self.frame.device}, {self.frame.backend.name})\n"
        return str_ + str(self.array)

    def __repr__(self):  # numpydoc ignore=RT01
        """Print artis array."""
        return str(self)

    # ndarray subclassing
    def __array_ufunc__(self, ufunc, method, *inputs, **kwargs):
        """
        Array universal function.

        Parameters
        ----------
        ufunc : object
            Ufunc object that was called.
        method : str
            Ufunc method that was called.
            (one of "__call__", "reduce", "reduceat", "accumulate", "outer",
             "inner").
        *inputs : tuple
            Ufunc arguments.
        **kwargs : dic
            Ufunc keyword arguments. If given, any out arguments, both
            positional and keyword, are passed as a tuple in kwargs.

        Returns
        -------
        array
            Artis array with the result.
        """

        array = self.array
        inputs, kwargs = self.frame.__extract_arrays__(inputs, kwargs)

        if hasattr(self.frame.backend, "__array_ufunc__"):
            output = self.frame.backend.__array_ufunc__(
                array, ufunc, method, *inputs, **kwargs
            )
        else:
            output = array.__array_ufunc__(ufunc, method, *inputs, **kwargs)

        if "out" in kwargs:
            return self
        return artis.Array(output)

    def __array_function__(self, func, types, *args, **kwargs):
        """
        Array universal function.

        Parameters
        ----------
        func : object
            Arbitrary callable exposed by NumPy’s public API.
        types : collection
            Collection of unique argument types from the original NumPy
            function call.
        *args : tuple
            Function arguments.
        **kwargs : dic
            Function keyword arguments.

        Returns
        -------
        array
            Artis array with the result.
        """
        args, kwargs = self.frame.__extract_arrays__(args, kwargs)
        types = self.frame.__extract_types__(types)
        array = self.array

        if hasattr(self.frame.backend, "__array_function__"):
            output = self.frame.backend.__array_function__(
                array, func, types, *args, **kwargs
            )
        else:
            output = array.__array_function__(func, types, *args, **kwargs)

        return artis.Array(output)

    def __array_finalize__(self, array):  # numpydoc ignore=PR01,RT01
        """
        Method called whenever the system internally allocates a new ndarray.
        """
        return artis.Array(array)

    def __array__(self, dtype=None):  # numpydoc ignore=RT01
        """
        To identify an object as an array.

        Parameters
        ----------
        dtype : type, optional
            Output data type.

        Returns
        -------
        array
            Array data.
        """

        if hasattr(self.frame.backend, "__array__"):
            output = self.frame.backend.__array__(dtype=dtype)
        elif dtype is not None:
            output = self.array.astype(dtype)
        else:
            output = self.array

        return output
