# pylint: disable=fixme
#
# Copyright (C) 2017 Joaquín Otón
#
# Authors: Joaquín Otón, Gabriel Jover Mañas
#
# This file is part of artis
#
# artis is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# artis is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with artis.  If not, see <http://www.gnu.org/licenses/>.
"""
Artis Frame.

Frame class allows to switch among several devices (cpu, cuda) using different
mathematical frameworks as numpy, cupy, torch ...
"""

from typing import Optional, overload

import artis


class Frame:
    """
    Frame defining the mathematical backend and device for the calculations.

    Parameters
    ----------
    device : str
        Active device name.
    framework : str
        Framework name to be used.

    Attributes
    ----------
    backend : BaseBackend
        Mathematical backend
    device :
        Device used to process the calculations
    """

    backend = None
    device = None

    # Properties
    @property
    def framework(self):  # numpydoc ignore=RT01
        """Framework name."""
        return self.backend.name

    # class methods
    @overload
    def __init__(self, frame: Optional["Frame"] = None):
        """
        Initialize the frame cloning device/backend from input frame.

        Parameters
        ----------
        frame : Frame
            Frame to clone.
        """

    @overload
    def __init__(self, device: Optional[str] = "cpu", framework=None):
        """
        Initialize the frame to specific device/backend.

        Parameters
        ----------
        device : str, optional, default: 'cpu'
            Active device name.
        framework : str, optional
            Framework name to be used. If not passed, it is obtained from
            artis.default_framework_for_device.
        """

    def __init__(self, device="cpu", framework=None):

        """
        Initialize the frame to specific device/backend.

        Parameters
        ----------
        device : str, optional, default: 'cpu'
            Active device name.
        framework : str, optional
            Framework name to be used. If not passed, it is obtained from
            artis.default_framework_for_device.
        """
        self._attrib_list = []
        self.device = None
        self.backend = None

        # If the frame is passed instead of the device
        if isinstance(device, self.__class__):
            frame = device
            self.device = frame.device
            self.backend = frame.backend
            self._attrib_list = frame._attrib_list.copy()
        else:
            self.set_device(device, framework)

    def set_device(self, device, framework=None):
        """
        Select specific device/backend.

        Parameters
        ----------
        device : str
            Active device.
        framework : str, optional
            Framework name to be used.
        """

        device, framework = self.find_frame(device, framework)
        self.device = device
        self.backend = artis.framework_catalog[framework]

    def find_frame(self, device, framework=None):
        """
        Get the corresponding backend and device.

        Parameters
        ----------
        device : str
            Device name.
        framework : str, optional
            Framework name to process with the device.

        Returns
        -------
        device : str
            Device name.
        backend : BaseBackend
            Framework backend compatible with the device.

        Raises
        ------
        Exception
            It raises an exception in case the backend is not registered
            for the device.
        """

        if framework is not None and framework not in artis.framework_catalog:
            raise ValueError(
                f"Framework {framework} not available. "
                f"Expected one of {artis.framework_catalog.keys()}"
            )

        if device is None:
            if framework is None:
                if self.backend is None:
                    device = "cpu"
                else:
                    device = self.backend.devices[0]
            else:
                device = artis.framework_catalog[framework].devices[0]
        elif device == "cuda":
            device += ":0"
        elif device not in artis.device_frameworks:
            raise ValueError(
                f"Device {device} not available. "
                f"Expected one of {artis.device_frameworks.keys()}"
            )

        framework_list = artis.device_frameworks[device]

        # TODO: warn that the backend has change
        if framework is None:
            framework = artis.default_framework_for_device[device]

        if framework not in framework_list:
            raise ValueError(
                f"Framework {framework} not available for device "
                f"{device}. Expected one of {framework_list}"
            )

        return device, framework

    def extract_frame(self, args, kwargs):  # numpydoc ignore=PR01
        """
        Extract device and backend from args and kwargs.

        Returns
        -------
        args
            Input args after removing device and backend.
        kwargs
            Input kwargs after removing device and backend.
        device
            Device extracted from input kwargs or current device.
        framewoek
            Framework name extracted from input kwargs or current framework.
        """
        framework = self.backend.name
        if "framework" in kwargs:
            framework = kwargs.pop("framework")

        device = self.device
        if "device" in kwargs:
            device = kwargs.pop("device")

        return args, kwargs, device, framework

    def __extract_arrays_recursive(self, arg):
        """
        Extract backend from artis arrays from passing arguments.

        If the argument is a tuple, tuple elements will be converted
        recursivelly.

        Parameters
        ----------
        arg : any
            Any argument.

        Returns
        -------
        any
            Argument with artis arrays converted to backend arrays.
        """
        if isinstance(arg, artis.Array):

            if self != arg.frame:
                array = self.backend.from_array(
                    arg.array, arg.frame.backend, self.device
                )
                return array
            else:
                return arg.array
        elif isinstance(arg, tuple):
            newargs = ()
            for obj in arg:
                newargs += (self.__extract_arrays_recursive(obj),)
            return newargs
        elif isinstance(arg, list):
            newargs = []
            for obj in arg:
                newargs += [
                    self.__extract_arrays_recursive(obj),
                ]
            return newargs
        else:
            return arg

    def __extract_types__(self, types):
        """
        Extract backend array types from a list of types.

        Elements in the list that are artis arrays are replaced by
        the undelying backend array type.

        Parameters
        ----------
        types : iterable
            Types to be converted.

        Returns
        -------
        tuple
            Types converted to backend array type.
        """
        newtypes = ()
        for type_ in types:
            if type_ == artis.Array:
                newtypes += (self.backend.array_class,)
            else:
                newtypes += (type_,)

        return newtypes

    def __extract_arrays__(self, args, kwargs):  # numpydoc ignore=PR01
        """
        Extract backend array from arguments and keyword arguments.

        Artis arrays are replaced by the undelying backend array.

        Returns
        -------
        list
            Arguments after converting artis arrays to backend arrays.
        dic
            Keyword arguments after converting artis arrays to backend arrays.
        """

        args = self.__extract_arrays_recursive(args)

        for key, value in kwargs.items():
            kwargs[key] = self.__extract_arrays_recursive(value)

        return args, kwargs

    # class customization
    def __getattr__(self, attr):  # numpydoc ignore=PR01,RT01
        """
        Catch the sequence of called attributes.

        Framework functions/attributes can be called and the frame stores
        the full sequence before calling the selected backend
        function/attribute.
        """
        frame = Frame(self)
        frame._attrib_list.append(attr)
        return frame

    def __call__(self, *args, **kwargs):  # numpydoc ignore=PR01,RT01
        """
        Redirect function calls to selected backend.

        Framework functions/attributes can be called through the frame.

        In case an array constructor method is called, active frame
        is used. The device and framework can also be selected by declaring
        them as arguments. Here, we remove them  from args and pass as
        parameters to the backend interface function caller.
        """

        args, kwargs, device, framework = self.extract_frame(args, kwargs)
        args, kwargs = self.__extract_arrays__(args, kwargs)

        device, framework = self.find_frame(device, framework)
        attrib_list = self._attrib_list

        if (
            framework in artis.framework_modules
            and attrib_list[0] in artis.framework_modules[framework]
        ):
            module = attrib_list.pop(0)
            backend = artis.framework_modules[framework][module]
        else:
            backend = artis.framework_catalog[framework]

        return backend.call(attrib_list, device, *args, **kwargs)

    def __dir__(self):  # numpydoc ignore=RT01
        """
        Return list of attribute names of active framework.

        For Ipython console purposes.
        """
        dir_ = dir(
            self.backend._recursive_getattr(
                self.backend.module, self._attrib_list
            )
        )
        dir_.append("set_device")
        dir_.append("find_frame")
        return dir_

    # pylint: disable=redefined-builtin
    def __str__(self):  # numpydoc ignore=RT01
        """Frame string formating."""
        str_ = f"Artis Frame({self.device}, {self.backend.name})"
        for attr in self._attrib_list:
            str_ += f".{attr}"
        return str_

    def __repr__(self):  # numpydoc ignore=RT01
        """Print the active frame."""
        return self.__str__()

    def __eq__(self, other):  # numpydoc ignore=PR01,RT01
        """Check if the frame is equal to other frame."""

        return (self.device == other.device) & (self.backend == other.backend)
