# Copyright (C) 2017 Joaquín Otón
#
# Authors: Joaquín Otón, Gabriel Jover Mañas
#
# This file is part of artis
#
# artis is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# artis is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with artis.  If not, see <http://www.gnu.org/licenses/>.
"""
Processing frame and mathematical backend base class.

This module makes it simple to choose a computational frame
used for the calculations.

The frame consist of a mathematical backend (as numpy, cupy or others)
and a device (as cpu or cuda:0) used for the calculations.
"""

from artis.framework.array import Array
from artis.framework.base_backend import BaseBackend
from artis.framework.frame import Frame

from artis.framework._cupy import CupyBackend  # isort:skip
from artis.framework._numpy import NumpyBackend  # isort:skip
from artis.framework._torch import TorchBackend  # isort:skip

__all__ = ["Frame", "BaseBackend", "Array"]
