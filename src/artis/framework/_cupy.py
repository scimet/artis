# Copyright (C) 2017 Joaquín Otón
#
# Authors: Joaquín Otón, Gabriel Jover Mañas
#
# This file is part of artis
#
# artis is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# artis is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with artis.  If not, see <http://www.gnu.org/licenses/>.
"""Cupy backend interface."""

import cupy as _cp  # pylint: disable=import-error

from artis.framework import BaseBackend


class CupyBackend(BaseBackend):
    """
    Cupy backend.
    """

    _name = "cupy"
    _module = _cp
    _array_class = _cp.ndarray
    # pylint: disable=c-extension-no-member
    _devices = [f"cuda:{n}" for n in range(_cp.cuda.runtime.getDeviceCount())]

    def get_device(self, array: _cp.ndarray):
        """
        Get device name from array.

        Parameters
        ----------
        array : ndarray
            Cupy ndarray.

        Returns
        -------
        str
            Device name.
        """
        return f"cuda:{array.device.id}"

    def from_array(self, array, backend=None, device=None, dtype=None):
        """
        Convert any backend array to Cupy array.

        Parameters
        ----------
        array : ndarray
            Input array related to any backend.
        backend : BaseBackend
            Framework backend of the input array. To be considered for
            conversion.
        device : str
            Target device for this backend. In this case, Cupy, only accepts
            'cuda' devices.
        dtype : TYPE, optional
            Dtype for output array. The default is None.

        Returns
        -------
        ndarray
            Copy or view of input array, depending on the backend.
        """
        if "cuda" in device:
            if len(device) > 5:
                _, number = device.split(":")
                device_number = int(number)
            else:
                device_number = 0
        else:
            raise ValueError(
                f"Device {device} not available. "
                f"Expected one of {self._devices}"
            )
        with _cp.cuda.Device(device_number):
            return _cp.asarray(array, dtype)

    def _get_result(self, attrib_list, device, *args, **kwargs):
        """
        Get operation result from the framwework.

        Method nested name is given by attrib_list. It uses the selected device
        with passed arguments.

        Parameters
        ----------
        attrib_list : list
            List of nested attributes.
        device : str
            Device name.
        *args : list
            Function arguments.
        **kwargs : dic
            Function keyword arguments.

        Returns
        -------
        any
            Operation's result.
        """
        # convert list to arrays
        for arg in args:
            new_args = ()
            if isinstance(arg, list):
                arg = self.list_to_array(arg)
            new_args += (arg,)
        args = new_args

        with _cp.cuda.Device(device[-1]):
            result = self._recursive_getattr(_cp, attrib_list)(*args, **kwargs)
            return result

    def list_to_array(self, list_):
        """
        Convert nested lists to array.

        Parameters
        ----------
        list_ : list
            List to be converted to an array.

        Returns
        -------
        array
            Array made by stacking list elements.
        """
        stack = []
        for value in list_:
            if isinstance(value, list):
                value = self.list_to_array(value)
            elif not isinstance(value, _cp.ndarray):
                value = _cp.array(value)
            stack.append(value)
        if len(stack) > 0:
            array = _cp.stack(stack)
        else:
            array = _cp.asarray([])
        return array
