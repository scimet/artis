# Copyright (C) 2017 Joaquín Otón
#
# Authors: Joaquín Otón, Gabriel Jover Mañas
#
# This file is part of artis
#
# artis is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# artis is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with artis.  If not, see <http://www.gnu.org/licenses/>.

import artis
from artis.framework import BaseBackend


def test_artis():
    assert isinstance(artis.frame, artis.Frame)
    assert isinstance(artis.framework_catalog, dict)
    for name, framework in artis.framework_catalog.items():
        assert isinstance(name, str)
        assert isinstance(framework, BaseBackend)
    assert isinstance(artis.default_framework_for_device, dict)
    for device, framework in artis.default_framework_for_device.items():
        assert isinstance(device, str)
        assert isinstance(framework, str)


def test_change_frame(framework_device_array):
    framework, device, array_class = framework_device_array
    artis.set_device("cpu", "numpy")
    array = artis.ones(4)
    assert isinstance(array, artis.Array)
    array.to_device(device, framework)
    artis.set_device(device, framework)
    assert type(array.array) == artis.frame.backend.array_class


def test_artis_calls(framework_device_array):
    framework, device, array_class = framework_device_array
    array = artis.ones((2, 2), device=device, framework=framework)
    array_type = type(array.array)
    assert array_type == array_class
