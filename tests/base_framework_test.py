# Copyright (C) 2017 Joaquín Otón
#
# Authors: Joaquín Otón, Gabriel Jover Mañas
#
# This file is part of artis
#
# artis is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# artis is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with artis.  If not, see <http://www.gnu.org/licenses/>.

import inspect

import numpy as _np
import pytest

from artis.framework import BaseBackend


def test_base_framework(backend):
    assert isinstance(backend.name, str)
    assert inspect.ismodule(backend.module)
    assert isinstance(backend.array_class, type)
    assert isinstance(backend.devices, list)
    for device in backend.devices:
        assert isinstance(device, str)


class NotImplementedBackend(BaseBackend):
    def __init__(self, remove_attribute):
        attrs = {
            "_name": "dummy",
            "_module": _np,
            "_array_class": _np.ndarray,
            "_devices": ["null"],
        }
        attrs.pop(remove_attribute)
        for name, value in attrs.items():
            setattr(self, name, value)
        super().__init__()

    def get_device(self, array):
        pass

    def to_device(self, array, device, framework=None):
        pass

    def call(self, attrib_list, device, *args, **kwargs):
        pass

    def _get_result(self, attrib_list, device, *args, **kwargs):
        pass

    def from_array(self, array, backend=None, device=None, dtype=None):
        pass


@pytest.mark.parametrize(
    "remove_attribute", ["_name", "_module", "_array_class", "_devices"]
)
def test_notimplemented_base_framework(remove_attribute):
    """
    If any attribute is not implemented it has to rise a NotImplemented error
    """
    with pytest.raises(NotImplementedError):
        NotImplementedBackend(remove_attribute)
