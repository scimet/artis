# Copyright (C) 2017 Joaquín Otón
#
# Authors: Joaquín Otón, Gabriel Jover Mañas
#
# This file is part of artis
#
# artis is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# artis is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with artis.  If not, see <http://www.gnu.org/licenses/>.

import pytest

import artis

_backend_list = []
_framework_list = []
_devices = []
_framework_arrays = []
_framework_device_array = []
for framework, backend in artis.framework_catalog.items():
    _backend_list.append(backend)
    _framework_list.append(backend.name)
    _framework_arrays.append((framework, backend.array_class))

    for device in backend.devices:
        _framework_device_array.append((framework, device, backend.array_class))
        if device not in _devices:
            _devices.append(device)


@pytest.fixture(params=_framework_device_array)
def framework_device_array(request):
    return request.param


framework_device_array_dest = framework_device_array


@pytest.fixture(params=_backend_list)
def backend(request):
    return request.param


@pytest.fixture(params=_framework_list)
def framework(request):
    return request.param


@pytest.fixture(params=_devices)
def device(request):
    return request.param


@pytest.fixture
def frame(framework_device_array):
    framework, device, array_class = framework_device_array
    frame = artis.Frame(device=device, framework=framework)
    return frame
