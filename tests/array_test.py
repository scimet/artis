# Copyright (C) 2017 Joaquín Otón
#
# Authors: Joaquín Otón, Gabriel Jover Mañas
#
# This file is part of artis
#
# artis is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# artis is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with artis.  If not, see <http://www.gnu.org/licenses/>.

import artis


def test_array(framework_device_array):
    framework, device, array_class = framework_device_array
    arsa = artis.Array(device=device, framework=framework)
    assert isinstance(arsa.array, artis.Array)
    assert isinstance(arsa.frame, artis.Frame)


def test_list_array():
    arsa = artis.Array([1, 2, 3])
    assert isinstance(arsa, artis.Array)
    assert arsa.shape == (3,)
    assert list(arsa) == [1, 2, 3]
    assert (arsa == [1, 2, 3]) == artis.Array([True, True, True])
    assert artis.all(arsa == [1, 2, 3])


def test_operations():
    arsa = artis.ones(3)
    assert isinstance(arsa, artis.Array)
    assert arsa.shape == (3,)

    arsa = artis.random.rand(3)
    assert isinstance(arsa, artis.Array)
    assert arsa.shape == (3,)

    arsa_abs = artis.abs(arsa)
    assert isinstance(arsa_abs, artis.Array)


def test_to_device(framework_device_array, framework_device_array_dest):
    framework, device, array_class = framework_device_array
    frame_dest, dev_dest, array_class_dest = framework_device_array_dest
    arsa = artis.ones((2, 2), device=device, framework=framework)
    arsa.to_device(dev_dest, frame_dest)
    assert arsa.frame.device == dev_dest
