# Copyright (C) 2017 Joaquín Otón
#
# Authors: Joaquín Otón, Gabriel Jover Mañas
#
# This file is part of artis
#
# artis is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# artis is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with artis.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import pytest


@pytest.fixture
def frame_not_pytorch(frame):
    if frame.framework not in ["numpy", "cupy"]:
        pytest.skip(f"Euler does not support {frame.framework}")
    return frame


@pytest.mark.parametrize("value", np.random.rand(10) * 1000)
def test_rotations(frame_not_pytorch, value):
    frame = frame_not_pytorch
    frame.euler.rot_x(value)
