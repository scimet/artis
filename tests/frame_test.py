# Copyright (C) 2017 Joaquín Otón
#
# Authors: Joaquín Otón, Gabriel Jover Mañas
#
# This file is part of artis
#
# artis is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# artis is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with artis.  If not, see <http://www.gnu.org/licenses/>.

import pytest

import artis
from artis.framework import BaseBackend


@pytest.mark.dependency()
def test_frame():
    ars = artis.Frame()
    assert isinstance(ars.backend, BaseBackend)
    assert isinstance(ars.device, str)
    assert isinstance(dir(ars), list)
    assert isinstance(str(ars), str)


@pytest.mark.dependency(depends=["test_frame"])
class TestFrame:
    def test_frame_init(self, framework_device_array):
        framework, device, _ = framework_device_array
        artis.Frame()
        ars = artis.Frame(device=device, framework=framework)
        ars2 = artis.Frame(ars)
        assert ars == ars2

    def test_copy_frame(self, framework_device_array):
        framework, device, array_class = framework_device_array
        arsa = artis.Frame(device=device, framework=framework)
        arsb = artis.Frame(arsa)
        assert arsa.backend == arsb.backend
        assert arsa.device == arsb.device

    def test_set_device(self, framework_device_array):
        framework, device, array_class = framework_device_array
        ars = artis.Frame()
        ars.set_device(device, framework)
        array = ars.ones((2, 2))
        array_type = type(array.array)
        assert array_type == array_class

    def test_set_device_on_call(self, framework_device_array):
        framework, device, array_class = framework_device_array
        ars = artis.Frame()
        array = ars.ones((2, 2), device=device, framework=framework)
        array_type = type(array.array)
        assert array_type == array_class

    def test_frame_getattr(self, framework_device_array):
        framework, device, array_class = framework_device_array
        ars = artis.Frame(device=device, framework=framework)
        ars_ones = ars.ones
        ars_ones = ars.ones
        array = ars_ones(2)
        assert type(array) == artis.Array
        array_type = type(array.array)
        assert array_type == array_class

    @pytest.mark.parametrize(
        "device, framework",
        [
            ("XXX", None),
            ("XXX", "numpy"),
            (None, "XXX"),
            ("cpu", "XXX"),
            ("cpu", "cupy"),
            ("cuda:0", "numpy"),
        ],
    )
    def test_find_frame_errors(self, device, framework):
        ars = artis.Frame()
        with pytest.raises(ValueError):
            ars.find_frame(device, framework)

    def test_passing_list_arguments(self, frame):
        a1 = frame.ones(3)
        a2 = frame.array([1, frame.sin(2), frame.cos(3)])
        frame.sin([a1, a2])
